#include <stdio.h>
#include <stdlib.h>

#define RGB_COLOUR 1


typedef struct Item
{
	int type;
	int redValue;
	int greenValue;
	int blueValue;
	int alpha;
};

int main(int argc, char** argv)
{
	Item BlueColour;
	BlueColour.type = RGB_COLOUR; 
	BlueColour.redValue = 0;
	BlueColour.greenValue = 0;
	BlueColour.blueValue = 255;
	BlueColour.alpha = 0;

	exit(0);
}