#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{

	int radius; 
	int diameter;
	int circumference;
	int area; 

	printf("Please enter the radius of the circle\n");
	scanf_s("%d", &radius);

	diameter = radius * 2; 
	printf("The diameter of the circle is: %d \n", diameter);

	circumference = diameter * 3.14159; 
	printf("The circumference of the circle is: %d \n", circumference);

	area = radius * radius * 3.14159; 
	printf("The area of the circle is: %d \n", area);

	exit(0);
}