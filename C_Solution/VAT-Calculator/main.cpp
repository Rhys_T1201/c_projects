#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	float price; 
	float addedVAT; 
	float totalPrice;

	printf("Please enter the price of the product\n");
	scanf_s("%f", &price); 

	addedVAT = price * 0.2; 
	totalPrice = price + addedVAT;

	printf("Price with added VAT %0.2f pound/s", totalPrice);

	exit(0);
}