#include <stdio.h>
#include <stdlib.h>

#define HEALTH_PACK 1
#define STD_SHIELD 2
#define STD_UBERSHIELD 3 

/* Pickup Item */
typedef struct Item
{
	int type;
	int value;
	int damage;
	int shield;
};

int main(int argc, char** argv)
{
	Item healthPack;
	healthPack.type = HEALTH_PACK;
	healthPack.value = 10;
	healthPack.damage = -100;
	healthPack.shield = 0;

	Item Shield;
	Shield.type = STD_SHIELD;
	Shield.value = 200;
	Shield.damage = -100;
	Shield.shield = 100;

	Item UberShield;
	UberShield.type = STD_UBERSHIELD; 
	UberShield.value = 200; 
	UberShield.damage = -100;
	UberShield.shield = 100;


	exit(0);
}