#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	int a;
	int b;
	int area;

	printf("Please enter the height of the triangle\n");
	scanf_s("%d", &a);

	printf("Please enter the width of the triangle\n");
	scanf_s("%d", &b);

	area = a * b / 2;
	printf("The Area of the triangle is: %dcm squared\n", area);

	exit(0);
}