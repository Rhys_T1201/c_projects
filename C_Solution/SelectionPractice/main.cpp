/* Selection Constructs
* if example
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	const int LIMIT = 5;

	int myNumber; /* create variable */
	scanf("%d", &myNumber); /* read in number */

	if (myNumber > LIMIT) /* if condition */
	{
		printf("bigger than five \n"); /* multi-line statement
		surrounded by {} */

		printf("I need another line\n");

		printf("just to make a point\n");
	}

	if (myNumber < 0)
	{
		printf("Subzero"); 
	}
	else
		printf("not bigger than five\n"); /* single line statement
		no need for {} */

	exit(0);
}