/*
* Basic "Hello World"
*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	printf("Hello World \n");
	printf("from \n");
	printf("Rhys Thomas \n");

	exit(0);
}